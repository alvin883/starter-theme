<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HeadLabNeo
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="the-site">

		<div id="header">
			<nav class="navbar navbar-expand-lg">
				<div class="container">

					<!-- Logo or something -->
					<div class="navbar-brand mb-0">
						<?php if (function_exists('the_custom_logo')) {
							the_custom_logo();
						}?>
					</div>

					<!-- Navigation toggler -->
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<i class="icon fas fa-bars"></i>
					</button>
					
					<!-- Wp navigation -->
					<div class="right-nav collapse navbar-collapse" id="navbarNav">
						<?php
							$args = array(
								'theme_location'=> 'primary',
								'depth'         => 2,
								'container'     => false,
								'menu_class'    => 'navbar-nav',
								'walker'        => new Bootstrap_Walker_Nav_Menu()
							);

							if (has_nav_menu('primary')) wp_nav_menu($args);
						?>
					</div>

				</div>
			</nav>
		</div>
