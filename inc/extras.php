<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package HeadLabNeo
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function headlab_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'headlab_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function headlab_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'headlab_pingback_header' );


/**
 * Add Bootstrap button classes to tag cloud
 */
function headlab_tag_cloud_btn( $return ) {
	$return = str_replace('<a', '<a class="btn btn-secondary btn-sm"', $return );
	return $return;
}
add_filter( 'wp_tag_cloud', 'headlab_tag_cloud_btn' );


/**
 * Customize the Read More Button
**/
function headlab_modify_read_more_link() {
    return sprintf( '<a class="more-link btn btn-sm btn-secondary" href="%1$s">%2$s</a>',
        get_permalink(),
        __( 'Read More', 'headlab' )
    );
}
add_filter( 'the_content_more_link', 'headlab_modify_read_more_link' );


/*
* Display Plain menu
* No <ul>, no <li>, just <a>
**/
function plain_menu($location) {
	$menuLocations = get_nav_menu_locations(); // This returns an array of menu locations;
  
	$menuID = $menuLocations[$location]; // Get the *MENU* menu ID
  
	$menu_navs = wp_get_nav_menu_items($menuID);
  
	$queried_page_id = get_queried_object_id();
	?>
		<?php
		foreach ( $menu_navs as $menu_nav ) {
  
		  $object_id = intval($menu_nav->object_id);
  
		  if ( $queried_page_id == $object_id ) {
			$active = " class='active' ";
		  } else {
			$active = '';
		  }
  
		  echo '<a href="'. esc_url( $menu_nav->url ) .'" '. $active .' title="'. esc_html( $menu_nav->title ) .'"><span>'. esc_html( $menu_nav->title ) .'</span></a>';
		}
		?>
	<?php 
  }


/* Add options ACF */
if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title'  => 'Theme General Settings',
		'menu_title'  => 'Theme Settings',
		'menu_slug'   => 'theme-settings',
		'capability'  => 'edit_posts',
		'redirect'    => false
	));

	acf_add_options_sub_page(array(
		'page_title'  => 'Homepage Settings',
		'menu_title'  => 'Homepage Setting',
		'parent_slug' => 'theme-settings',
	));
}


/* Post Pagination */
add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');
function next_posts_link_attributes(){
	return 'class="next page-numbers"';
} 
function prev_posts_link_attributes(){
  	return 'class="prev page-numbers"';
}


// WARNING : this function will remove the default wordpress CSS
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');