<?php
/* Custom WP native gallery output*/
function customFormatGallery($string, $attr){

  $output = '<div class="row gallery-native">';
  $posts = get_posts(
    array(
      'include'   => $attr['ids'],
      'post_type' => 'attachment',
      'order'     => 'DESC',
      'orderby'   => 'post__in'
    )
  );

  foreach($posts as $imagePost){

      if(empty($attr['size'])){ $attr['size'] = null; }

      $col_to_bootstrap = array(
        1 => 12,
        2 => 6,
        3 => 4,
        4 => 3,
        5 => 3,
        6 => 2,
        7 => 2,
        8 => 2,
        9 => 2,
        10 => 2,
        11 => 2,
        12 => 1
      );

      // Convert to bootstrap column size
      $columns = (isset($attr['columns'])) ? $col_to_bootstrap[$attr['columns']] : 4;
      // Prevent Column size > image length
      if(isset($attr['columns']) && count($posts) < $attr['columns'] ) $columns = $col_to_bootstrap[count($posts)];

      $output .= "<div class='column-img col-12 col-sm-6 col-lg-". $columns ."'>
          <a class='gallery-columns-" . $columns . "' href='" . wp_get_attachment_image_src($imagePost->ID, 'full')[0] . "' title='" . get_the_title($imagePost->ID) . "'>
            <img src='" . wp_get_attachment_image_src($imagePost->ID, $attr['size'])[0] . "'/>
          </a>
        </div>";
  }

  $output .= "</div>";

  return $output;
}
add_filter('post_gallery','customFormatGallery',10,2);
?>