<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package HeadLabNeo
 */

get_header(); ?>

	<div id="page-404">
		<div class="section header-404">
			<div class="container">
				<div class="content-set centered">
					<h1 class="title">404 Not Found !</h1>
					<div class="content">
						<?php _e('Sorry, we can\'t find the page you are looking for, you entering empty state .', 'headlab'); ?>
					</div>
					<a href="<?php echo get_home_url(); ?>" class="btn">
						Go to Homepage
					</a>
				</div>
			</div>
		</div>
	</div><!-- #page-404 -->
	
<?php
get_footer();
