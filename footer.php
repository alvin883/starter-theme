<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package headlab
 */

?>
		<footer id="footer">
			<div class="container">

				<!-- Copyright & Wappstars -->
				<div class="bottom">
					<div>
						&copy; Copyright <?php echo date("Y"); ?>
						<a href="<?php echo home_url('/'); ?>">
							<?php bloginfo('name'); ?>
						</a>
					</div>
					<div>
						<?php _e('Designed by ', 'headlab'); ?>
						<a href="https://www.wappstars.nl/" target="_blank">
							Wappstars B.V.
						</a>
					</div>
				</div>
				
			</div>
		</footer>

		<?php wp_footer(); ?>
	</div><!-- #the-site -->
</body>
</html>
