<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package HeadLabNeo
 */

get_header(); ?>

	<div id="archive">

		<!-- Header -->
		<div class="section page-header no-thumbnail">
			<div class="container">
				<div class="content-set">
					<h1 class="title">
						<?php
							if(is_year()) {
								_e('Archive: Year ','headlab');
								the_time('Y');
							} else if(is_month()) {
								_e('Archive: ','headlab');
								the_time('F Y');
							} else if(is_date()) {
								_e('Archive: ','headlab');
								the_time('d F Y');
							} else if(is_category()) {
								_e('Category: ','headlab');
								single_term_title();
							} else if(is_author()) {
								_e('Author: ','headlab');
								the_author();
							}
						?>
					</h1>
					<div class="content">
                        <?php echo 'Showing ' . $GLOBALS['wp_query']->post_count . ' post'; ?>
					</div>
				</div>
			</div>
		</div>

		<!-- List of posts -->
		<div class="section posts">
			<div class="container">
				<div class="row">

					<?php if ( have_posts() ) :
						while ( have_posts() ) : the_post(); ?>

							<div class="col-12 col-md-6 col-lg-4">
								<a href="<?php the_permalink(); ?>" class="post-item">
									<div class="post-item-wrapper">
										<?php
											$photoClass = !has_post_thumbnail() ? 'no-thumbnail' : '';
											$photoAttr = has_post_thumbnail() ? 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\');"' : '';
											$photoInnerHTML = !has_post_thumbnail() ? '<i class="icon fas fa-images"></i>' : '';
										?>
										<div class="image <?php echo $photoClass; ?>" <?php echo $photoAttr; ?>>
											<?php echo $photoInnerHTML; ?>
										</div>
										<div class="content-set">
											<h4 class="title">
												<?php the_title(); ?>
											</h4>
											<div class="date">
												<?php the_time('F j, Y'); ?>
											</div>
											<div class="content">
												<?php echo strip_shortcodes(wp_trim_words(get_the_content(), 15, '...')); ?>
											</div>
										</div>
									</div>
								</a>
							</div>

						<?php endwhile;
					else : ?>
						<div class="post-item-empty col-12">
							<div class="content-set">
								<h3 class="title">
									😥
								</h3>
								<div class="content">
									<h4>
										<?php _e('Niet Gevonden!', 'headlab'); ?>
									</h4>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div><!--.row-->
			</div><!--.container-->
		</div>

        <?php get_template_part('template-parts/components/pagination'); ?>

	</div>

<?php get_footer();