<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package HeadLabNeo
 */

?>

<div id="page-404">

	<!-- Header -->
	<div class="section header-404">
		<div class="container">
			<div class="content-set centered">
				<h1 class="title">
					<?php _e('404 not found!','headlab'); ?>
				</h1>
	
				<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

					<div class="content">
						<?php _e('Ready to publish your first post?', 'headlab'); ?>
					</div>
					<a href="<?php echo esc_url( admin_url( 'post-new.php' ) );?>" class="btn">
						<?php _e('Get started here', 'headlab'); ?>
					</a>

				<?php elseif ( is_search() ) : ?>

					<div class="content">
						<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.'	, 'headlab' ); ?></p>
						<?php get_search_form(); ?>
					</div>

				<?php else : ?>

					<div class="content">
						<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'headlab' ); ?></p>
						<?php get_search_form();?>
					</div>

				<?php endif; ?>

			</div>
		</div>
	</div>

</div>