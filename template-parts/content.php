<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package HeadLabNeo
 */

?>

	<!-- Header -->
	<?php
		$thumbAttr = has_post_thumbnail() ? ' style="background-image: url(\'' . get_the_post_thumbnail_url() . '\');"' : '';
		$thumbClass = !has_post_thumbnail() ? ' no-thumbnail' : '';
	?>
	<div class="section page-header <?php echo $thumbClass; ?>" <?php echo $thumbAttr; ?>>
		<div class="container">
			<div class="content-set">
				<h1 class="title">
					<?php the_title(); ?>
				</h1>
				<?php 
					// Hide post info if this is a `Page`
					if ( 'post' === get_post_type() ) : ?>
						<div class="content posted-on">
							<?php headlab_posted_on(); ?>
						</div>
				<?php else : ?>
					<?php if(function_exists('get_field') && get_field('page_subtitle')) : ?>
						<div class="content">
							<?php the_field('page_subtitle'); ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<!-- The Content -->
	<div class="section wp-the-content">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 mx-auto" id="the-content">
					<?php the_content(); ?>
					<?php 
						// Hide JsSocials if this is a `Page` type
						if ( 'post' === get_post_type() ) : ?>
							<div class="js-social-wrapper">
								<h3 class="title">
									<?php _e('Share this', 'headlab'); ?>
								</h3>
								<div id="js-social-buttons"></div>
							</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<!-- Pagination -->
	<?php 
		wp_link_pages(
			array(
				'before'      => '
					<div class="post-navigation">
						<div class="all-page-number post-pagination">
							<span class="info">' . __('Pages:', 'headlab') . '</span>',
				'after'       => '</div></div>',
				'link_before' => '<span">',
				'link_after'  => '</span>',
				'pagelink'    => '%',
				'separator'   => ' ',
			)
		);
	?>