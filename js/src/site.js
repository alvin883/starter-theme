jQuery(document).ready(function($){

    /** Swipebox */
    $( '.gallery-native, .wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox();
    });
    
    /** JS Social */
    $("#js-social-buttons").jsSocials({
        shares: [
            {
                share: 'twitter',
                logo: 'fab fa-twitter'
            },
            {
                share: 'facebook',
                logo: 'fab fa-facebook-f'
            },
            {
                share: 'pinterest',
                logo: 'fab fa-pinterest-p'
            },
            {
                share: 'linkedin',
                logo: 'fab fa-linkedin'
            }
        ],
        showLabel: false,
        showCount: false
    });
    
});